﻿using Microsoft.AspNetCore.Mvc;
using Servicos;

namespace Apresentacao
{
    [Route("/api/[Controller]")]
    [ApiController]
    public class PizzariaController:ControllerBase
    {
        private readonly IServPizzaria _servPizzaria; 
        public PizzariaController(IServPizzaria servPizzaria)
        {
            _servPizzaria = servPizzaria;
        }

        [HttpPost]
        public ActionResult Inserir(InserirPizzariaDTO inserirPizzariadDTO)
        {
            try
            {
                _servPizzaria.Inserir(inserirPizzariadDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
