﻿using Entidades;
using Repositorio;

namespace Servicos
{
    public interface IServPizzaria
    {
        void Inserir(InserirPizzariaDTO inserirPizzariaDTO);
    }
    public class ServPizzaria:IServPizzaria
    {
        private readonly IRepoPizzaria _pizzariaRepository;
        public ServPizzaria(IRepoPizzaria pizzariaRepository)
        {
                _pizzariaRepository = pizzariaRepository;
        }

        public void Inserir(InserirPizzariaDTO inserirPizzariaDTO)
        {
            var pizzaria = new Pizzaria();

            pizzaria.Nome = inserirPizzariaDTO.Nome;
            pizzaria.Endereco = inserirPizzariaDTO.Endereco;
            pizzaria.Telefone = inserirPizzariaDTO.Telefone;

            _pizzariaRepository.Inserir(pizzaria);
        }
    }
}
